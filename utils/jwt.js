const config = require("config");
const jwt = require("jsonwebtoken");

const {
  secret,
  tokenDuration,
} = config.app;

module.exports = {
  encode,
  decode,
};

function encode(json, options = {}) {
  options.expiresIn = tokenDuration;
  return jwt.sign(json, secret, options);
}

function decode(token) {
  const tokenOnly = token.replace("Bearer ", "");
  return jwt.verify(tokenOnly, secret);
}
