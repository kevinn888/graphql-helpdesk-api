const login = require("./login");
const me = require("./me");
const recordListInfo = require("./recordListInfo");
const user = require("./user");
const createIssue = require("./createIssue");
const category = require("./category");
const reports = require("./reports");
const comment = require("./comment");

module.exports = {
  user,
  recordListInfo,
  login,
  me,
  createIssue,
  category,
  comment,
  reports,
};
