const { CommentFacade } = require("../facades");
const { render } = require("../utils");

module.exports = {
  list,
  create,
  get,
  update,
  find,
  destroy,
};

async function list() {
  const result = await CommentFacade.list();
  return render("comment", result);
}

async function get(req) {
  const { id } = req.params;
  const result = await CommentFacade.get(id);
  return render("comment", result);
}

async function find(req) {
  const { params } = req;
  const result = await CommentFacade.getCommentByEmail(params.id);
  return render("comment", result);
}

async function create(req) {
  const { body } = req;
  const result = await CommentFacade.create(body);
  return render("comment", result);
}

async function update(req) {
  const { id } = req.params;
  const result = await CommentFacade.update(req.body, id);
  return result;
}

async function destroy(req) {
  const { id } = req.params;
  const result = await CommentFacade.destroy(id);
  return render("comment", result);
}
