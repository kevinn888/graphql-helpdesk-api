const { ReportFacade } = require("../facades");

const {
  render,
} = require("../utils");

module.exports = {
  create,
  update,
  destroy,
  list,
  find,
};

async function create(report) {
  const { body } = report;
  const result = await ReportFacade.create(body);
  return render("reports", result);
}

async function update(req) {
  const { body, params } = req;
  const result = await ReportFacade.update(body, params.id);
  return result;
}

async function destroy(req) {
  const { params } = req;
  const result = await ReportFacade.destroy(params.id);
  return render("reports", result);
}

async function list() {
  const result = await ReportFacade.list();
  return render("reports", result);
}

async function find(req) {
  const { params } = req;
  const result = await ReportFacade.findReportByTicket(params.id);
  return render("reports", result);
}
