const { CategoryFacade } = require("../facades");
const { render } = require("../utils");

module.exports = {
  list,
  create,
  get,
  update,
  destroy,
};

async function list() {
  const result = await CategoryFacade.list();
  return render("category", result);
}

async function get(req) {
  const { id } = req.params;
  const result = await CategoryFacade.get(id);
  return render("category", result);
}

async function create(req) {
  const { body } = req;
  const result = await CategoryFacade.create(body);
  return render("category", result);
}

async function update(req) {
  const { id } = req.params;
  const result = await CategoryFacade.update(req.body, id);
  return result;
}

async function destroy(req) {
  const { id } = req.params;
  const result = await CategoryFacade.destroy(id);
  return render("category", result);
}
