const {
  SessionFacade,
} = require("../facades");
const {
  render,
} = require("../utils");

module.exports = {
  login,
  info,
  register,
};

async function login(request) {
  const {
    email,
    password,
  } = request.body;
  const result = await SessionFacade.login(email, password);
  return render("login", result);
}

async function info(request) {
  const { user } = request.context;
  return render("me", user);
}

async function register(request) {
  const { body } = request;
  const result = await SessionFacade.register(body);
  return result;
}
