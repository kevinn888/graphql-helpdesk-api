const { UserFacade } = require("../facades");

const {
  render,
} = require("../utils");

module.exports = {
  list,
  create,
  find,
  update,
  destroy,
};

async function create(req) {
  const { body } = req;
  const result = await UserFacade.create(body);
  return render("user", result);
}

async function list() {
  const result = await UserFacade.list();
  return render("user", result);
}

async function find(req) {
  const { params } = req;
  const result = await UserFacade.getUserByEmail(params.id);
  return render("user", result);
}

async function destroy(req) {
  const { params } = req;
  const result = await UserFacade.deleteUser(params.id);
  return result;
}

async function update(req) {
  const { body, params } = req;
  const result = await UserFacade.updateUser(body, params.id);
  return result;
}
