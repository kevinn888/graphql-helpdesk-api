const RootController = require("./RootController");
const SessionController = require("./SessionController");
const UserController = require("./UserController");
const CategoryController = require("./CategoryController");
const ReportController = require("./ReportController");
const CommentController = require("./CommentController");

module.exports = {
  SessionController,
  RootController,
  UserController,
  CategoryController,
  ReportController,
  CommentController,
};
