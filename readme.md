# ITHB Web Tech Api Server

## Prerequisites
1. NodeJS ^10
1. NPM ^6
1. PostgreSQL ^10

## How to install
1. Git clone from repository
1. run ```npm install```
1. for doing some test whether the setup is already correct or not, execute ```npm run test```

## Development mode
1. Run ```npm run watch```

# Setting Up Database
The database is configured under ```config/default.js```. This configuration can be overidden by the value of environment variable ```NODE_ENV``` as specified in (npm config)[https://www.npmjs.com/package/config#quick-start]

