const { sha256 } = require("../utils");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: "email cannot be empty",
          },
          isEmail: {
            msg: "Please enter a valid email address",
          },
        },
        unique: {
          args: true,
          msg: "your email already registered",
        },
      },
      role: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: "role cannot be empty",
          },
        },
      },
      phoneNumber: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: "phone number cannot be empty",
          },
          not: {
            args: ["[a-z]", "i"],
            msg: "cannot contain letter",
          },
        },
      },
      password: DataTypes.STRING,
      rawPassword: {
        type: DataTypes.VIRTUAL,
        set(val) {
          this.setDataValue("rawPassword", val);
          this.setDataValue("password", sha256(val));
        },
        validate: {
          notEmpty: {
            msg: "password cannot be empty",
          },
        },
      },
    },
    {},
  );
  // User.associate = function associate(models) {
  // associations can be defined here
  // };
  Object.assign(User.prototype, {
    authenticationData,
  });

  User.hashPassword = function hashPassword(value) {
    return sha256(value);
  };

  function authenticationData() {
    return {
      id: this.id,
      email: this.email,
      password: this.password,
    };
  }

  User.associate = (models) => {
    User.belongsToMany(models.Category, { through: "Reports", foreignKey: "id" });
    User.belongsToMany(models.Category, { through: "Reports", foreignKey: "handle", as: "CustomerService" });
  };
  return User;
};
