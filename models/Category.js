module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define(
    "Category",
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      category: DataTypes.STRING,
      categoryName: DataTypes.STRING,
    },
    {},
  );
  return Category;
};
