module.exports = (sequelize, DataTypes) => {
  const Report = sequelize.define(
    "Report",
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      ticket: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "Ticket cannot be empty",
          },
        },
      },
      description: DataTypes.STRING,
      status: DataTypes.INTEGER,
      subject: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "Subject cannot be empty",
          },
        },
      },
      category: {
        type: DataTypes.UUID,
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "Category cannot be empty",
          },
        },
      },
      handle: DataTypes.UUID,
      dateAndTime: DataTypes.DATE,
      userId: {
        type: DataTypes.UUID,
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "User Id cannot be empty",
          },
        },
      },
    },
  );
  Report.associate = (models) => {
    Report.belongsTo(models.User, { foreignKey: "userId" });
    Report.belongsTo(models.Category, { foreignKey: "category" });
    Report.belongsTo(models.User, { foreignKey: "handle" });
  };
  return Report;
};
