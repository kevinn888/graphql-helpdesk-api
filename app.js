require("dotenv").config();
const config = require("config");
const express = require("express");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const middlewares = require("./app/middlewares");
const mainRouter = require("./app/mainRouter");

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(logger("dev"));
middlewares(app);
app.use("/", mainRouter.router);

app.use((err, req, res, next) => {
  let statusCode = !err.statusCode ? "400" : err.statusCode;
  statusCode = err.type === "loginFailed" ? "401" : statusCode;
  res.status(statusCode);
  let errorJSON = {};
  if (statusCode === "400" && err.errors) {
    errorJSON = err.errors.map(error => ({ path: error.path, msg: error.message }));
  } else if (statusCode === "401") {
    errorJSON = {
      msg: "your email/password is incorrect",
    };
  } else if (statusCode === "400" && err.type === "RequiredException") {
    errorJSON = {
      type: err.type,
      msg: err.detail.message,
    };
  } else if (statusCode === "400" && err.type === "notFound") {
    errorJSON = {
      type: err.type,
      msg: `${err.detail.id} not found`,
    };
  } else if (err.type === "invalidToken") {
    errorJSON = {
      type: err.type,
      msg: "your token is invalid/expired",
    };
  } else if (err.name === "SequelizeConnectionError") {
    errorJSON = { type: err.name, msg: "connection error" };
  } else {
    errorJSON = {
      msg: "undocumented error",
    };
  }

  if (config.app.responseShowErrorTrace) {
    errorJSON.trace = err.stackArray;
  }
  if (config.app.showError) {
    // eslint-disable-next-line no-console
    console.error(err);
  }
  res.json(errorJSON);
  next();
});

module.exports = app;
