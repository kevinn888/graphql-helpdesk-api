const Exception = require("./Exception");

module.exports = class RequiredException extends Exception {
  constructor(value, message) {
    super("RequiredException", { value, message });
  }

  static check(fieldName, value, message) {
    if (value === undefined || value === null) throw new RequiredException(fieldName, message);
  }
};
