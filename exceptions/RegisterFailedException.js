const Exception = require("./Exception");

module.exports = class RegisterFailed extends Exception {
  constructor() {
    super("registerFailed");
  }
};
