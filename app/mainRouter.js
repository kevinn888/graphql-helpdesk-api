const {
  Router,
} = require("../modules");
const {
  RootController,
  SessionController,
  UserController,
  ReportController,
  CommentController,
  CategoryController,
} = require("../controllers");

const router = new Router();

router.get("/info", RootController.info);
router.post("/login", SessionController.login);
router.post("/register", SessionController.register);

// User Route
router.rest("/v1/users", UserController);

router.get("/me", SessionController.info);

router.rest("/v1/categories", CategoryController);

// Report Route
router.rest("/v1/reports", ReportController);

// comment route
router.rest("/v1/comments", CommentController);

module.exports = router;
