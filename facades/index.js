const SessionFacade = require("./SessionFacade");
const UserFacade = require("./UserFacade");
const CategoryFacade = require("./CategoryFacade");
const ReportFacade = require("./ReportFacade");
const CommentFacade = require("./CommentFacade");

module.exports = {
  SessionFacade,
  UserFacade,
  CategoryFacade,
  ReportFacade,
  CommentFacade,
};
