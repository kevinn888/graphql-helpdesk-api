const { User } = require("../models");
const { NotFoundException } = require("../exceptions");

module.exports = {
  list,
  create,
  find,
  getUserByEmail,
  deleteUser,
  updateUser,
};

async function create(data) {
  const result = await User.create(data);
  return result;
}

async function list() {
  const records = await User.findAll();
  return records;
}

async function find(id) {
  const result = await User.findOne({
    where: {
      id,
    },
  });
  return result;
}

async function getUserByEmail(email) {
  const result = await User.findOne({
    where: {
      email,
    },
  });
  if (!result) { throw new NotFoundException(email, "reports"); }
  return result;
}

async function deleteUser(email) {
  const result = await User.destroy({
    where: {
      email,
    },
  });
  return result;
}

async function updateUser({
  email, name, phoneNumber, password,
}, id) {
  const result = await User.update({
    name,
    email,
    phoneNumber,
    rawPassword: password,
  }, {
    where: {
      email: id,
    },
  });
  return result;
}
