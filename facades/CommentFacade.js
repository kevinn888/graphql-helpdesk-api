const { Comment } = require("../models");
const { NotFoundException } = require("../exceptions");

module.exports = {
  list,
  getCommentByEmail,
  get,
  create,
  update,
  destroy,
};

async function create(data) {
  const result = await Comment.create(data);
  return result;
}

async function get(id) {
  const record = await Comment.findByPk(id);
  if (!record) { throw new NotFoundException(id, "Comment"); }
  return record;
}

async function list() {
  const records = await Comment.findAll();
  return records;
}

async function getCommentByEmail(email) {
  const result = await Comment.findAll({
    where: {
      email,
    },
  });
  if (!result) { throw new NotFoundException(email, "reports"); }
  return result;
}

async function destroy(id) {
  const result = await Comment.destroy({
    where: {
      id,
    },
  });
  return result;
}

async function update(data, id) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}
