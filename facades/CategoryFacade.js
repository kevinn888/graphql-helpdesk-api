const { Category } = require("../models");
const { NotFoundException } = require("../exceptions");

module.exports = {
  list,
  get,
  getById,
  create,
  update,
  destroy,
};

async function create(data) {
  const result = await Category.create(data);
  return result;
}

async function get(data) {
  const record = await Category.findOne({
    where: {
      category: data,
    },
  });
  if (!record) { throw new NotFoundException(data, "Category"); }
  return record;
}

async function getById(data) {
  const record = await Category.findOne({
    where: {
      id: data,
    },
  });
  if (!record) { throw new NotFoundException(data, "Category"); }
  return record;
}

async function list() {
  const records = await Category.findAll();
  return records;
}

async function update({ categoryName }, id) {
  const cat = id.toUpperCase();
  const result = await Category.update({
    categoryName,
  }, {
    where: {
      id: cat,
    },
  });
  return result;
}

async function destroy(id) {
  const result = await Category.destroy({
    where: {
      id,
    },
  });
  return result;
}
