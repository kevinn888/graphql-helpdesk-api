const {
  User,
} = require("../models");
const {
  RequiredException,
  LoginFailedException,
  RegisterFailedException,
} = require("../exceptions");
const {
  jwt,
} = require("../utils");

module.exports = {
  getUserByAuthentication,
  login,
  register,
};

async function getUserByAuthentication(authentication) {
  if (!authentication) return null;
  const {
    id,
    email,
    password,
  } = authentication;
  if (!id || !password || !email) return null;
  return User.findOne({
    where: {
      id,
      email,
      password,
    },
  });
}

async function login(email, rawPassword) {
  const message = "your email / password must be filled";
  RequiredException.check("email", email, message);
  RequiredException.check("rawPassword", rawPassword, message);
  const user = await User.findOne({
    where: {
      email,
      password: User.hashPassword(rawPassword),
    },
  });
  if (!user) throw new LoginFailedException();
  return {
    token: jwt.encode({
      email,
      password: user.password,
      id: user.id,
    }),
  };
}

async function register(req) {
  let message;
  req.role = "user";
  const result = await User.create(req);
  if (!result.dataValues) {
    throw new RegisterFailedException();
  } else {
    message = 1;
  }
  // console.log(result);
  return message;
}
