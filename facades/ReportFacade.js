const Sequelize = require("sequelize");
const { Report, User } = require("../models");
const { get, getById } = require("./CategoryFacade");
const { find } = require("./UserFacade");
const { NotFoundException } = require("../exceptions");

const { Op } = Sequelize;

module.exports = {
  list,
  findReportByTicket,
  create,
  update,
  destroy,
  getUserByEmail,
};

async function getUserByEmail(data) {
  const result = await User.findOne({
    where: {
      email: data,
    },
  });
  if (!result) { throw new NotFoundException(data, "reports"); }
  return result;
}

async function create(report) {
  const ticketNum = await count(report.category);
  let ticketString = "";
  if (ticketNum < 10) {
    ticketString = `${report.category}-00${ticketNum}`;
  } else if (ticketNum < 100) {
    ticketString = `${report.category}-0${ticketNum}`;
  } else {
    ticketString = report.category + ticketNum;
  }
  const getUser = await getUserByEmail(report.userId);
  const getCategory = await get(report.category);
  const ticketReport = {
    ...report,
    ticket: ticketString,
    userId: getUser.id,
    category: getCategory.id,
  };
  const result = await Report.create(ticketReport);

  return result;
}

async function list() {
  const records = await Report.findAll({
    where: {
      [Op.or]: [{ status: 1 }, { status: 0 }],
    },
  });
  const response = await Promise.all(records.map(async (val) => {
    const categoryData = await getById(val.category);
    let handleData;
    if (val.handle) {
      handleData = await find(val.handle);
    }
    const userData = await find(val.userId);
    const {
      id,
      ticket,
      description,
      status,
      subject,
      dateAndTime,
      createdAt,
      updatedAt,
    } = val;

    const record = {
      id,
      ticket,
      description,
      status,
      subject,
      dateAndTime,
      createdAt,
      updatedAt,
      category: categoryData.categoryName,
      userId: userData.email,
      handle: handleData ? handleData.email : null,
    };
    return record;
  }));
  return response;
}

async function findReportByTicket(ticket) {
  const result = await Report.findOne({
    where: {
      ticket,
    },
  });
  return result;
}

async function destroy(ticket) {
  const result = await Report.destroy({
    where: {
      ticket,
    },
  });
  return result;
}

async function update({
  description, dateAndTime, ticket, status, handle,
}, id) {
  const getHandle = await getUserByEmail(handle);
  const result = await Report.update({
    description,
    dateAndTime,
    ticket,
    status,
    handle: getHandle.id,
  }, {
    where: {
      ticket: id,
    },
  });
  return result;
}

async function count(id) {
  const categoryData = await get(id);
  const result = await Report.findAndCountAll({
    where: {
      category: categoryData.id,
    },
  });
  return result.count;
}
